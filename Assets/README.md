# Facebook

## What

* Facebook sdk for unity v7.18.0 + Jar v1.2.124

## Installation

```bash
"com.yenmoc.facebook":"https://gitlab.com/yenmoc/facebook"
or
npm publish --registry http://localhost:4873
```

